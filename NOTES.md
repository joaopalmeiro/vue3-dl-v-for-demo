# Notes

- https://piccalil.li/quick-tip/dl-grid/
- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dl
- https://v3-migration.vuejs.org/breaking-changes/key-attribute.html#with-template-v-for
- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dt (Description Term)
- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dd (Description Details)
- https://vuejs.org/guide/essentials/list.html#maintaining-state-with-key

## Commands

```bash
npm install vue && npm install -D vite @vitejs/plugin-vue typescript vue-tsc create-vite-tsconfigs sort-package-json npm-run-all2 prettier
```
