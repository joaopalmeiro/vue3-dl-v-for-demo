# vue3-dl-v-for-demo

A [description list](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dl) rendered with the [`v-for` directive](https://vuejs.org/guide/essentials/list.html#v-for-on-template) in Vue 3.

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version
```

```bash
npm install
```

```bash
npm run dev
```

```bash
npm run lint
```

```bash
npm run format
```
